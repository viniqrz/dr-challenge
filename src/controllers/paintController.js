const paintService = require("../services/paintService");

const calcGallons = (req, res, next) => {
  try {
    const dto = req.body;

    const result = paintService.calcGallons(dto);
  
    res
      .status(200)
      .json({
        status: 'success',
        data: result
      });
  } catch(err) {
    next(err);
  }
}

const paintController = { calcGallons };

module.exports = paintController;