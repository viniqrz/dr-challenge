const { Router } = require('express');

const paintController = require('../controllers/paintController');
const { validateBody } = require('../middlewares/validateBody');

const paintRouter = Router();

paintRouter
  .route('/calc-gallons')
  .post(validateBody, paintController.calcGallons);

module.exports = paintRouter;
