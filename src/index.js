const express = require('express');

const { createGlobalErrorHandler } = require('./config/server/createGlobalErrorHandler');
const { createMiddlewares } = require('./config/server/createMiddlewares');
const { createRoutes } = require('./config/server/createRoutes');
const { runServer } = require('./config/server/runServer');

const app = express();

const start = (app) => {

  createMiddlewares(app);
  createRoutes(app);
  createGlobalErrorHandler(app);
  runServer(app);

}

start(app);