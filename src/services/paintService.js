const AppError = require("../config/exceptions/AppError")

const DOOR_HEIGHT = 1.9;
const DOOR_AREA = 1.52;
const WINDOW_AREA = 2.4;
const GALLON_SIZES = [18, 3.6, 2.5, 0.5];

const validateWallArea = (wallArea, index) => {
  if (wallArea < 1 || wallArea > 15) {
    throw new AppError(400, `${index + 1}-nth wall has invalid area`);
  }
}

const validateDoorsAndWindowsArea = (doors, windows, wallArea) => {
  const doorsAndWindowsArea = DOOR_AREA * doors + WINDOW_AREA * windows;

  if (doorsAndWindowsArea > (0.5 * wallArea)) {
    throw new AppError(400, `Doors and windows can have at max 50% of wall area`);
  }
}

const validateWallHeight = (doors, wallHeight) => {
  if (!doors) return;

  if (wallHeight < (DOOR_HEIGHT + 0.3)) {
    throw new AppError(400, `Wall height must be at least 0.3m taller than door`);
  }
}

const calcGallons = (dto) => {
  const { walls } = dto;

  let totalArea = 0;

  walls.forEach(({ height, width, doors, windows }, index) => {
    const wallArea = height * width;
    const doorsArea = DOOR_AREA * doors;
    const windowsArea = WINDOW_AREA * doors;
    const paintingArea = wallArea - (doorsArea + windowsArea);

    validateWallArea(wallArea, index);
    validateDoorsAndWindowsArea(doors, windows, wallArea);
    validateWallHeight(doors, height);

    totalArea += paintingArea;
  });

  let litersLeft = totalArea * 5;
  let gallonIndex = 0;
  const result = [];

  while (litersLeft > 0 && gallonIndex < 4) {
    const gallon = GALLON_SIZES[gallonIndex];
    const remainder = litersLeft - gallon;

    if (gallon > litersLeft) {
      gallonIndex += 1;

      if (gallonIndex === 3) result.push(gallon); 

      continue;
    }

    litersLeft = remainder;
    result.push(gallon);
  }

  return result;
}

const paintService = { calcGallons };

module.exports = paintService;
