const AppError  = require('../config/exceptions/AppError');

exports.validateBody = (req, res, next) => {
  try {
    if (Object.keys(req.body).length === 0) throw new AppError(400, 'No body sent!');

    if (req.body.walls.length !== 4) throw new AppError(400, 'Must have 4 walls');

    next();
  } catch(err) {
    next(err);
  }
}