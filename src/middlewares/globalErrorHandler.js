exports.globalErrorHandler = (err, req, res, next) => {
  console.log(err);

  res
    .status(err.statusCode || 400)
    .json({
      status: "fail",
      data: err.message,
    });
}