const morgan = require("morgan");
const express = require("express");

exports.createMiddlewares = (app) => {
  app.use(morgan("dev"));
  app.use(express.json());
}