const paintRouter = require("../../routes/paintRouter")

exports.createRoutes = (app) => {
  app.use('/paint', paintRouter);
}
