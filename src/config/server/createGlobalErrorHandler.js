const { globalErrorHandler } = require('../../middlewares/globalErrorHandler');

exports.createGlobalErrorHandler = (app) => {
  app.use(globalErrorHandler);
}
