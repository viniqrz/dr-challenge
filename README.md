<h1 align="center">Welcome to Digital Republic Challenge 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D5.5.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D9.3.0-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Challenge for digital republic

## Prerequisites

- npm >=5.5.0
- node >=9.3.0

## Install

```sh
npm install
```

## Usage

```sh
npm run dev
```

## Request Example

```javascript
{

  // url: http://localhost:5000/paint/calc-gallons
  // method: POST

  // Body
	"walls": [
		{ "height": 2.5, "width": 5, "doors": 1, "windows": 1 },
		{ "height": 2.5, "width": 5, "doors": 1, "windows": 1 },
		{ "height": 2.5, "width": 5, "doors": 1, "windows": 1 },
		{ "height": 2.5, "width": 5, "doors": 1, "windows": 1 }
	]

  // Response
  {
  "status": "success",
  "data": [
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    3.6,
    3.6,
    2.5,
    0.5,
    0.5,
    0.5,
    0.5
  ]
}
}
```

## Author

👤 **Vinicius Queiroz**

* Website: linkedin.com/in/viniqrz
* Github: [@viniqrz](https://github.com/viniqrz)
* LinkedIn: [@viniqrz](https://linkedin.com/in/viniqrz)

## Show your support

Give a ⭐️ if this project helped you!
